# Adyen Coding Assignment

## Available Scripts

In the project directory, you can run:

### `yarn`

Installs the required libraries. This command should be called once at the start, and after adding/removing new libraries into `package.json` file.

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn test`

Runs all the tests and display the results.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

## Possible Improvements

There are definitely a lot of points to improve. Some of them are;

- Components need a lot more testing. Currently, I am only doing snapshot tests for the components, and it is not covering every possible scenario now
- Also, end-to-end test will make the life easier, current code also does not have end-to-end tests
- I used sass for css generation, but actually I hadn't used a lot of css codes in this project.
- Getting the weather and flight info is working fine, but I haven't covered all the possibilities, there should be some more error handling there.
- I used `create-react-app` command for creating the boilerplate, but in a real project, it will be better to eject from it and configure it manually.
- I had not divided the code into a lot of components, it is normally the recommended way, but I did not do that because of time concerns.
- More optimization. But, optimization processes does not finish in practice, you need to finish it at some point, otherwise you cannot deliver your code.
- In a big project, code splitting will also be a good idea. I haven't applied it here, I think it is not necessary for this project
- More responsiveness will always fine. This code has it but not because I added it but because of the ui library I used in this project had it built-in.
- Code coverage of this project is fine, but it can be better.

## Notes

- The api endpoints i used has some rate limiting, so there is a possibility that it wont work because you reached your daily limits. If that will happen, please contact with me, i will deliver new endpoints to get that data.
