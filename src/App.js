import React, {Fragment} from "react";
import { Router } from "@reach/router";
import { useSelector } from "react-redux";

import LoginPage from "./pages/LoginPage";
import DashboardPage from "./pages/DashboardPage";

import "./App.scss";

function App() {
  const authenticatedUser = useSelector(({ authentication }) => authentication.user);

  return (
    <Router className="router">
      {!authenticatedUser && (
        <Fragment>
          <LoginPage default />
        </Fragment>
      )}

      {authenticatedUser && (
        <Fragment>
          <DashboardPage default />
        </Fragment>
      )}
    </Router>
  );
}

export default App;
