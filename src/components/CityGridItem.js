import React, {Fragment} from "react";
import PropType from "prop-types";
import {Avatar, Button, Card, CardActions, CardContent, CircularProgress, Grid, Typography} from "@material-ui/core";

import "./CityGridItem.scss";

export default function CityGridItem({ title, flag, onClick, data, reserved = false }) {
  const isLoading = data.weatherPending || data.flightsPending;

  // TODO Handle error states too

  return (
    <Grid className="city-grid-item" item={true} xs={12} md={4}>
      <Card variant="outlined">
        <CardContent>
          <Grid className="city-grid-item__title" container={true} direction="row" alignItems="center" justify="space-between">
            <Typography component="p" variant="h6">{title}</Typography>
            <Avatar className="city-grid-item__flag" variant="rounded" src={flag} />
          </Grid>

          {isLoading && (
            <Grid container={true} direction="column" alignItems="center" justify="center">
              <CircularProgress variant="indeterminate" color="secondary" size={24} />
              <Typography component="div" variant="subtitle1">Loading data...</Typography>
            </Grid>
          )}

          {!isLoading && data.weather && data.weather.data.length && (
            <Grid className="city-grid-item__weather" container={true} direction="column">
              <Typography component="p" variant="overline">Expected Weather Condition</Typography>
              <Typography component="p" variant="body2">{data.weather.data[0].weather.description}</Typography>
              <Typography component="p" variant="body2">{data.weather.data[0].high_temp} &#8451; Max. Temparature</Typography>
              <Typography component="p" variant="body2">{data.weather.data[0].low_temp} &#8451; Min. Temparature</Typography>
            </Grid>
          )}

          {!isLoading && data.flights && (
            <Grid className="city-grid-item__weather" container={true} direction="column">
              <Typography component="p" variant="overline">Cheapest Flight to Destination</Typography>

              {data.flights.data.length ? (
                <Fragment>
                  <Typography component="p" variant="body2">{data.flights.data[0].price} EUR</Typography>
                  <Typography component="p" variant="body2">{data.flights.data[0].availability.seats || "No"} Seats Available</Typography>
                </Fragment>
              ) : (
                <Typography component="p" variant="body2" color="error">No available flight for today</Typography>
              )}
            </Grid>
          )}
        </CardContent>

        <CardActions>
          {!isLoading && (
            <Button disabled={reserved} fullWidth={true} variant="outlined" onClick={onClick}>
              {reserved ? "Reserved" : "Reserve"}
            </Button>
          )}
        </CardActions>
      </Card>
    </Grid>
  );
}

CityGridItem.propTypes = {
  title: PropType.string.isRequired,
  flag: PropType.string.isRequired,
  reserved: PropType.bool,
  data: PropType.shape({
    weather: PropType.object,
    flights: PropType.object,

    weatherPending: PropType.bool,
    weatherError: PropType.object,

    flightsPending: PropType.bool,
    flightsError: PropType.object,
  }).isRequired,
  onClick: PropType.func.isRequired,
};
