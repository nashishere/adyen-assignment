import React from "react";
import {mount} from "enzyme";

import CityGridItem from "./CityGridItem";

const defaultProps = {
  title: "Amsterdam",
  flag: "1234567890",
  reserved: false,
  data: {
    weather: {
      data: [
        {
          weather: {
            description: "foo",
          },
          high_temp: 10,
          low_temp: 5,
        },
      ],
    },
    flights: {
      data: [
        {
          price: 100,
          availability: {
            seats: 4,
          },
        },
      ],
    },

    weatherPending: false,
    weatherError: null,

    flightsPending: false,
    flightsError: null,
  },
  onClick: () => {},
};

describe("CityGridItem component", () => {
  // TODO Need to add more tests here

  it("should render the component correctly with given parameters", function () {
    const wrapper = mount(<CityGridItem {...defaultProps} />);
    expect(wrapper).toMatchSnapshot();
  });
});
