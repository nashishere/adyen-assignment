import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import { store } from "./state";
import App from "./App";

import "./index.scss";

const component = (
  <Provider store={store}>
    <App />
  </Provider>
);

const container = document.getElementById("root");

ReactDOM.render(component, container);
