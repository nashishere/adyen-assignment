import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Container, Grid, Typography} from "@material-ui/core";

import {CITY} from "../utils/constants";
import {resourceActionCreators} from "../state";
import CityGridItem from "../components/CityGridItem";

import amsterdamFlag from "../assets/amsterdam.png";
import madridFlag from "../assets/madrid.png";
import budapestFlag from "../assets/budapest.png";

import "./DashboardPage.scss";

export default function DashboardPage() {
  const dispatch = useDispatch();
  const authenticatedUser = useSelector(({ authentication }) => authentication.user);

  const reservedCity = useSelector(({ resource }) => resource.reservedCity);

  const cities = useSelector(({ resource }) => ({
    amsterdam: resource[CITY.AMSTERDAM],
    madrid: resource[CITY.MADRID],
    budapest: resource[CITY.BUDAPEST],
  }));

  useEffect(() => {
    // TODO This may be better, like one dispatch for getting all the data

    dispatch(resourceActionCreators.getWeatherInfo(CITY.AMSTERDAM));
    dispatch(resourceActionCreators.getFlightsInfo(CITY.AMSTERDAM));

    dispatch(resourceActionCreators.getWeatherInfo(CITY.MADRID));
    dispatch(resourceActionCreators.getFlightsInfo(CITY.MADRID));

    dispatch(resourceActionCreators.getWeatherInfo(CITY.BUDAPEST));
    dispatch(resourceActionCreators.getFlightsInfo(CITY.BUDAPEST));
  }, [dispatch]);

  const handleReserveClick = (city) => {
    dispatch(resourceActionCreators.reserve(city));
  };

  const handleReserveAmsterdamClick = (event) => {
    event.preventDefault();

    handleReserveClick(CITY.AMSTERDAM);
  };

  const handleReserveMadridClick = (event) => {
    event.preventDefault();

    handleReserveClick(CITY.MADRID);
  };

  const handleReserveBudapestClick = (event) => {
    event.preventDefault();

    handleReserveClick(CITY.BUDAPEST);
  };

  return (
    <div className="dashboard-page">
      <Container component="main">
        <Typography component="h2" variant="h4">
          Hello, {authenticatedUser.firstName}
        </Typography>

        <Typography component="p" variant="body1">
          Please reserve your desk in one of the available spot
        </Typography>

        <Grid className="dashboard-page__list" container={true} spacing={3}>
          <CityGridItem
            title="Amsterdam"
            flag={amsterdamFlag}
            reserved={reservedCity === CITY.AMSTERDAM}
            data={cities.amsterdam}
            onClick={handleReserveAmsterdamClick} />

          <CityGridItem
            title="Madrid"
            flag={madridFlag}
            reserved={reservedCity === CITY.MADRID}
            data={cities.madrid}
            onClick={handleReserveMadridClick} />

          <CityGridItem
            title="Budapest"
            flag={budapestFlag}
            reserved={reservedCity === CITY.BUDAPEST}
            data={cities.budapest}
            onClick={handleReserveBudapestClick} />
        </Grid>
      </Container>
    </div>
  );
}
