import React from "react";
import {mount} from "enzyme";

import DashboardPage from "./DashboardPage";
import {Provider} from "react-redux";
import {createMockStore} from "../utils/utils";

describe("DashboardPage component", () => {
  // TODO Need to add more tests and snapshots here

  it("should render the component correctly with given parameters", function () {
    const store = createMockStore({
      authentication: {
        user: {
          firstName: "Jane",
        }
      }
    });

    const wrapper = mount(
      <Provider store={store}>
        <DashboardPage />
      </Provider>
    );

    expect(wrapper).toMatchSnapshot();
  });
});
