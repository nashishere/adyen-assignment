import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {
  Box,
  Button,
  Container,
  Dialog, DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
  Typography
} from "@material-ui/core";

import {authenticationActionCreators} from "../state";
import {validateLoginForm} from "../utils/utils";

import "./LoginPage.scss";

export default function LoginPage() {
  const dispatch = useDispatch();

  const {isPending, error} = useSelector(({ authentication }) => ({
    isPending: authentication.loginPending,
    error: authentication.loginError,
  }));

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [formErrors, setFormErrors] = useState();
  const [isErrorDialogOpen, setIsErrorDialogOpen] = useState(false);

  useEffect(() => {
    if (error) {
      setIsErrorDialogOpen(true);
    }
  }, [error]);

  const handleEmailChange = (event) => {
    setFormErrors(undefined);
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setFormErrors(undefined);
    setPassword(event.target.value);
  };

  const handleSubmitClick = (event) => {
    event.preventDefault();

    const validationResult = validateLoginForm({ email, password });

    if (validationResult) {
      setFormErrors(validationResult);
    } else {
      dispatch(authenticationActionCreators.login(email, password));
    }
  };

  const handleErrorDialogClose = () => {
    setIsErrorDialogOpen(false);
  };

  return (
    <Box className="login-page">
      <Container component="main" maxWidth="xs">
        <Typography component="h1" variant="h5">Sign In</Typography>
        <form noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            value={email}
            inputMode="email"
            onChange={handleEmailChange}
            error={formErrors && formErrors.email !== undefined}
            helperText={formErrors && formErrors.email}
          />

          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            value={password}
            inputMode="text"
            onChange={handlePasswordChange}
            error={formErrors && formErrors.password !== undefined}
            helperText={formErrors && formErrors.password}
          />

          <Button
            className="login-page__submit"
            type="button"
            fullWidth
            variant="contained"
            color="primary"
            disabled={isPending}
            onClick={handleSubmitClick}>
            Sign In
          </Button>
        </form>
      </Container>

      <Dialog
        open={isErrorDialogOpen}
        onClose={handleErrorDialogClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        <DialogTitle id="alert-dialog-title">Error</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {error && error.message}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button color="primary" autoFocus onClick={handleErrorDialogClose}>OK</Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
}
