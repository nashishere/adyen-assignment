import React from "react";
import {mount} from "enzyme";

import LoginPage from "./LoginPage";
import {Provider} from "react-redux";
import {createMockStore} from "../utils/utils";


describe("LoginPage component", () => {
  // TODO Need to add more tests and snapshots here

  it("should render the component correctly with given parameters", function () {
    const store = createMockStore({
      authentication: {
        user: null,
      }
    });

    const wrapper = mount(
      <Provider store={store}>
        <LoginPage />
      </Provider>
    );

    expect(wrapper).toMatchSnapshot();
  });
});
