export const actionTypes = {
  LOGIN: "LOGIN",
  LOGIN_FULFILLED: "LOGIN_FULFILLED",
  LOGIN_FAILED: "LOGIN_FAILED",

  LOGOUT: "LOGOUT",
  LOGOUT_FULFILLED: "LOGOUT_FULFILLED",
  LOGOUT_FAILED: "LOGOUT_FAILED",
};

const login = (email, password) => ({
  type: actionTypes.LOGIN,
  payload: {
    email,
    password,
  },
});

const loginFulfilled = (user) => ({
  type: actionTypes.LOGIN_FULFILLED,
  payload: {
    user,
  },
});

const loginFailed = (error) => ({
  type: actionTypes.LOGIN_FAILED,
  payload: {
    error,
  },
});

const logout = () => ({
  type: actionTypes.LOGOUT,
});

const logoutFulfilled = () => ({
  type: actionTypes.LOGOUT_FULFILLED,
});

const logoutFailed = (error) => ({
  type: actionTypes.LOGOUT_FAILED,
  payload: {
    error,
  },
});

export const actionCreators = {
  login,
  loginFulfilled,
  loginFailed,

  logout,
  logoutFulfilled,
  logoutFailed,
};
