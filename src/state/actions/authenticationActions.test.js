import {authenticationActionCreators, authenticationActionTypes} from "../index";
import {TEST_USER} from "../../utils/constants";

describe("authenticationActions", () => {
  it("should create an action to login with given payload", () => {
    expect(authenticationActionCreators.login(TEST_USER.email, TEST_USER.password)).toEqual({
      type: authenticationActionTypes.LOGIN,
      payload: {
        email: TEST_USER.email,
        password: TEST_USER.password,
      },
    });
  });

  it("should create an action to login fulfilled with given payload", () => {
    expect(authenticationActionCreators.loginFulfilled(TEST_USER)).toEqual({
      type: authenticationActionTypes.LOGIN_FULFILLED,
      payload: {
        user: TEST_USER,
      },
    });
  });

  it("should create an action to login failed with given payload", () => {
    const error = new Error();

    expect(authenticationActionCreators.loginFailed(error)).toEqual({
      type: authenticationActionTypes.LOGIN_FAILED,
      payload: {
        error: error,
      },
    });
  });

  it("should create an action to logout with no payload", () => {
    expect(authenticationActionCreators.logout()).toEqual({
      type: authenticationActionTypes.LOGOUT,
    });
  });

  it("should create an action to logout fulfilled with no payload", () => {
    expect(authenticationActionCreators.logoutFulfilled()).toEqual({
      type: authenticationActionTypes.LOGOUT_FULFILLED,
    });
  });

  it("should create an action to logout failed with given payload", () => {
    const error = new Error();

    expect(authenticationActionCreators.logoutFailed(error)).toEqual({
      type: authenticationActionTypes.LOGOUT_FAILED,
      payload: {
        error: error,
      },
    });
  });
});
