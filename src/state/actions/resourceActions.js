export const actionTypes = {
  GET_WEATHER_INFO: "GET_WEATHER_INFO",
  GET_WEATHER_INFO_FULFILLED: "GET_WEATHER_INFO_FULFILLED",
  GET_WEATHER_INFO_FAILED: "GET_WEATHER_INFO_FAILED",

  GET_FLIGHTS_INFO: "GET_FLIGHTS_INFO",
  GET_FLIGHTS_INFO_FULFILLED: "GET_FLIGHTS_INFO_FULFILLED",
  GET_FLIGHTS_INFO_FAILED: "GET_FLIGHTS_INFO_FAILED",

  RESERVE: "RESERVE",
};

const getWeatherInfo = (city) => ({
  type: actionTypes.GET_WEATHER_INFO,
  payload: {
    city,
  },
});

const getWeatherInfoFulfilled = (city, data) => ({
  type: actionTypes.GET_WEATHER_INFO_FULFILLED,
  payload: {
    city,
    data,
  },
});

const getWeatherInfoFailed = (city, error) => ({
  type: actionTypes.GET_WEATHER_INFO_FAILED,
  payload: {
    city,
    error,
  },
});

const getFlightsInfo = (city) => ({
  type: actionTypes.GET_FLIGHTS_INFO,
  payload: {
    city,
  },
});

const getFlightsInfoFulfilled = (city, data) => ({
  type: actionTypes.GET_FLIGHTS_INFO_FULFILLED,
  payload: {
    city,
    data,
  },
});

const getFlightsInfoFailed = (city, error) => ({
  type: actionTypes.GET_FLIGHTS_INFO_FAILED,
  payload: {
    city,
    error,
  },
});

const reserve = (city) => ({
  type: actionTypes.RESERVE,
  payload: {
    city,
  },
});

export const actionCreators = {
  getWeatherInfo,
  getWeatherInfoFulfilled,
  getWeatherInfoFailed,

  getFlightsInfo,
  getFlightsInfoFulfilled,
  getFlightsInfoFailed,

  reserve,
};
