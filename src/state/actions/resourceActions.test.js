import {resourceActionCreators, resourceActionTypes} from "../index";
import {CITY} from "../../utils/constants";

describe("resourceActions", () => {
  it("should create an action to get weather info with given payload", () => {
    expect(resourceActionCreators.getWeatherInfo(CITY.AMSTERDAM)).toEqual({
      type: resourceActionTypes.GET_WEATHER_INFO,
      payload: {
        city: CITY.AMSTERDAM,
      },
    });
  });

  it("should create an action to get weather info fulfilled with given payload", () => {
    expect(resourceActionCreators.getWeatherInfoFulfilled(CITY.AMSTERDAM, {})).toEqual({
      type: resourceActionTypes.GET_WEATHER_INFO_FULFILLED,
      payload: {
        city: CITY.AMSTERDAM,
        data: {},
      },
    });
  });

  it("should create an action to get weather info failed with given payload", () => {
    const error = new Error();

    expect(resourceActionCreators.getWeatherInfoFailed(CITY.AMSTERDAM, error)).toEqual({
      type: resourceActionTypes.GET_WEATHER_INFO_FAILED,
      payload: {
        city: CITY.AMSTERDAM,
        error: error,
      },
    });
  });

  it("should create an action to get flights info with given payload", () => {
    expect(resourceActionCreators.getFlightsInfo(CITY.AMSTERDAM)).toEqual({
      type: resourceActionTypes.GET_FLIGHTS_INFO,
      payload: {
        city: CITY.AMSTERDAM,
      },
    });
  });

  it("should create an action to get flights info fulfilled with given payload", () => {
    expect(resourceActionCreators.getFlightsInfoFulfilled(CITY.AMSTERDAM, {})).toEqual({
      type: resourceActionTypes.GET_FLIGHTS_INFO_FULFILLED,
      payload: {
        city: CITY.AMSTERDAM,
        data: {},
      },
    });
  });

  it("should create an action to get flights info failed with given payload", () => {
    const error = new Error();

    expect(resourceActionCreators.getFlightsInfoFailed(CITY.AMSTERDAM, error)).toEqual({
      type: resourceActionTypes.GET_FLIGHTS_INFO_FAILED,
      payload: {
        city: CITY.AMSTERDAM,
        error: error,
      },
    });
  });

  it("should create an action to reserve a city with given payload", () => {
    expect(resourceActionCreators.reserve(CITY.AMSTERDAM)).toEqual({
      type: resourceActionTypes.RESERVE,
      payload: {
        city: CITY.AMSTERDAM,
      },
    });
  });
});
