import { createStore, applyMiddleware, combineReducers, compose } from "redux";
import { all } from "redux-saga/effects";
import createSagaMiddleware from "redux-saga";

import {
  actionTypes as authenticationActionTypes,
  actionCreators as authenticationActionCreators,
} from "./actions/authenticationActions";

import {
  actionTypes as resourceActionTypes,
  actionCreators as resourceActionCreators,
} from "./actions/resourceActions";

import {
  initialState as authenticationInitialState,
  reducer as authenticationReducer,
} from "./reducers/authenticationReducer";

import {
  initialState as resourceInitialState,
  reducer as resourceReducer,
} from "./reducers/resourceReducer";

import authenticationSagas from "./sagas/authenticationSagas";
import resourceSagas from "./sagas/resourceSagas";

const combinedReducers = combineReducers({
  authentication: authenticationReducer,
  resource: resourceReducer,
});

const initialApplicationState = {
  authentication: authenticationInitialState,
  resource: resourceInitialState,
};

const sagaMiddleware = createSagaMiddleware();

const store = createStore(combinedReducers, initialApplicationState, compose(applyMiddleware(sagaMiddleware)));

function* allSagas() {
  yield all([
    ...authenticationSagas,
    ...resourceSagas,
  ]);
}

sagaMiddleware.run(allSagas);

export {
  store,
  combinedReducers,
  authenticationActionTypes,
  authenticationActionCreators,
  authenticationInitialState,
  authenticationReducer,
  resourceActionTypes,
  resourceActionCreators,
  resourceInitialState,
  resourceReducer,
};
