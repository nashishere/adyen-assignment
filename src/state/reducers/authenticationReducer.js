import {actionTypes} from "../actions/authenticationActions";

export const initialState = {
  user: null,

  loginPending: false,
  loginError: null,

  logoutPending: false,
  logoutError: null,
};

export const reducer = (state, { type, payload = {} }) => {
  let newState = state ? { ...state } : { ...initialState };

  switch (type) {
    case actionTypes.LOGIN: {
      newState = {
        ...newState,
        loginPending: true,
        loginError: null,
      };

      break;
    }

    case actionTypes.LOGIN_FULFILLED: {
      newState = {
        ...newState,
        loginPending: false,
        loginError: null,
        user: payload.user,
      };

      break;
    }

    case actionTypes.LOGIN_FAILED: {
      newState = {
        ...newState,
        loginPending: false,
        loginError: payload.error,
      };

      break;
    }

    case actionTypes.LOGOUT: {
      newState = {
        ...newState,
        logoutPending: true,
        logoutError: null,
      };

      break;
    }

    case actionTypes.LOGOUT_FULFILLED: {
      newState = {
        ...newState,
        logoutPending: false,
        logoutError: null,
        user: null,
      };

      break;
    }

    case actionTypes.LOGOUT_FAILED: {
      newState = {
        ...newState,
        logoutPending: false,
        logoutError: payload.error,
      };

      break;
    }

    default: {
      break;
    }
  }

  return newState;
};
