import {authenticationActionCreators, authenticationInitialState, authenticationReducer} from "../index";
import {TEST_USER} from "../../utils/constants";

const initialState = { ...authenticationInitialState };

describe("authenticationReducer", () => {
  it("should return the initial state", () => {
    expect(authenticationReducer(undefined, {}))
      .toEqual(initialState);
  });

  it("should handle LOGIN action", () => {
    expect(authenticationReducer(initialState, authenticationActionCreators.login(TEST_USER.email, TEST_USER.password)))
      .toEqual({
        ...initialState,
        loginPending: true,
        loginError: null,
      });
  });

  it("should handle LOGIN_FULFILLED action", () => {
    expect(authenticationReducer(initialState, authenticationActionCreators.loginFulfilled(TEST_USER)))
      .toEqual({
        ...initialState,
        loginPending: false,
        loginError: null,
        user: TEST_USER,
      });
  });

  it("should handle LOGIN_FAILED action", () => {
    const error = new Error();

    expect(authenticationReducer(initialState, authenticationActionCreators.loginFailed(error)))
      .toEqual({
        ...initialState,
        loginPending: false,
        loginError: error,
      });
  });

  it("should handle LOGOUT action", () => {
    expect(authenticationReducer(initialState, authenticationActionCreators.logout()))
      .toEqual({
        ...initialState,
        logoutPending: true,
        logoutError: null,
      });
  });

  it("should handle LOGOUT_FULFILLED action", () => {
    expect(authenticationReducer(initialState, authenticationActionCreators.logoutFulfilled()))
      .toEqual({
        ...initialState,
        logoutPending: false,
        logoutError: null,
        user: null,
      });
  });

  it("should handle LOGOUT_FAILED action", () => {
    const error = new Error();

    expect(authenticationReducer(initialState, authenticationActionCreators.logoutFailed(error)))
      .toEqual({
        ...initialState,
        logoutPending: false,
        logoutError: error,
      });
  });
});
