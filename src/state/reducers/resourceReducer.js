import { actionTypes } from "../actions/resourceActions";
import { CITY } from "../../utils/constants";

export const initialState = {
  reservedCity: null,

  [CITY.AMSTERDAM]: {
    weather: null,
    flights: null,

    weatherPending: false,
    weatherError: null,

    flightsPending: false,
    flightsError: null,
  },

  [CITY.BUDAPEST]: {
    weather: null,
    flights: null,

    weatherPending: false,
    weatherError: null,

    flightsPending: false,
    flightsError: null,
  },

  [CITY.MADRID]: {
    weather: null,
    flights: null,

    weatherPending: false,
    weatherError: null,

    flightsPending: false,
    flightsError: null,
  },
};

export const reducer = (state, { type, payload = {} }) => {
  let newState = state ? { ...state } : { ...initialState };

  switch (type) {
    case actionTypes.GET_WEATHER_INFO: {
      newState = {
        ...newState,
        [payload.city]: {
          ...newState[payload.city],
          weatherPending: true,
          weatherError: null,
        }
      };

      break;
    }

    case actionTypes.GET_WEATHER_INFO_FULFILLED: {
      newState = {
        ...newState,
        [payload.city]: {
          ...newState[payload.city],
          weatherPending: false,
          weatherError: null,
          weather: payload.data,
        }
      };

      break;
    }

    case actionTypes.GET_WEATHER_INFO_FAILED: {
      newState = {
        ...newState,
        [payload.city]: {
          ...newState[payload.city],
          weatherPending: false,
          weatherError: payload.error,
        }
      };

      break;
    }

    case actionTypes.GET_FLIGHTS_INFO: {
      newState = {
        ...newState,
        [payload.city]: {
          ...newState[payload.city],
          flightsPending: true,
          flightsError: null,
        }
      };

      break;
    }

    case actionTypes.GET_FLIGHTS_INFO_FULFILLED: {
      newState = {
        ...newState,
        [payload.city]: {
          ...newState[payload.city],
          flightsPending: false,
          flightsError: null,
          flights: payload.data,
        }
      };

      break;
    }

    case actionTypes.GET_FLIGHTS_INFO_FAILED: {
      newState = {
        ...newState,
        [payload.city]: {
          ...newState[payload.city],
          flightsPending: false,
          flightsError: payload.error,
        }
      };

      break;
    }

    case actionTypes.RESERVE: {
      newState = {
        ...newState,
        reservedCity: payload.city,
      };

      break;
    }

    default: {
      break;
    }
  }

  return newState;
};
