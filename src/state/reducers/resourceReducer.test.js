import {resourceActionCreators, resourceInitialState, resourceReducer} from "../index";
import {CITY} from "../../utils/constants";

const initialState = { ...resourceInitialState };

describe("resourceReducer", () => {
  it("should return the initial state", () => {
    expect(resourceReducer(undefined, {})).toEqual(initialState);
  });

  it("should handle GET_WEATHER_INFO action", () => {
    expect(resourceReducer(initialState, resourceActionCreators.getWeatherInfo(CITY.AMSTERDAM)))
      .toEqual({
        ...initialState,
        [CITY.AMSTERDAM]: {
          ...initialState[CITY.AMSTERDAM],
          weatherPending: true,
          weatherError: null,
        },
      });
  });

  it("should handle GET_WEATHER_INFO_FULFILLED action", () => {
    expect(resourceReducer(initialState, resourceActionCreators.getWeatherInfoFulfilled(CITY.AMSTERDAM, {})))
      .toEqual({
        ...initialState,
        [CITY.AMSTERDAM]: {
          ...initialState[CITY.AMSTERDAM],
          weatherPending: false,
          weatherError: null,
          weather: {},
        },
      });
  });

  it("should handle GET_WEATHER_INFO_FAILED action", () => {
    const error = new Error();

    expect(resourceReducer(initialState, resourceActionCreators.getWeatherInfoFailed(CITY.AMSTERDAM, error)))
      .toEqual({
        ...initialState,
        [CITY.AMSTERDAM]: {
          ...initialState[CITY.AMSTERDAM],
          weatherPending: false,
          weatherError: error,
        },
      });
  });

  it("should handle GET_FLIGHTS_INFO action", () => {
    expect(resourceReducer(initialState, resourceActionCreators.getFlightsInfo(CITY.AMSTERDAM)))
      .toEqual({
        ...initialState,
        [CITY.AMSTERDAM]: {
          ...initialState[CITY.AMSTERDAM],
          flightsPending: true,
          flightsError: null,
        },
      });
  });

  it("should handle GET_FLIGHTS_INFO_FULFILLED action", () => {
    expect(resourceReducer(initialState, resourceActionCreators.getFlightsInfoFulfilled(CITY.AMSTERDAM, {})))
      .toEqual({
        ...initialState,
        [CITY.AMSTERDAM]: {
          ...initialState[CITY.AMSTERDAM],
          flightsPending: false,
          flightsError: null,
          flights: {},
        },
      });
  });

  it("should handle GET_FLIGHTS_INFO_FAILED action", () => {
    const error = new Error();

    expect(resourceReducer(initialState, resourceActionCreators.getFlightsInfoFailed(CITY.AMSTERDAM, error)))
      .toEqual({
        ...initialState,
        [CITY.AMSTERDAM]: {
          ...initialState[CITY.AMSTERDAM],
          flightsPending: false,
          flightsError: error,
        },
      });
  });

  it("should handle RESERVE action", () => {
    expect(resourceReducer(initialState, resourceActionCreators.reserve(CITY.AMSTERDAM)))
      .toEqual({
        ...initialState,
        reservedCity: CITY.AMSTERDAM,
      });
  });
});
