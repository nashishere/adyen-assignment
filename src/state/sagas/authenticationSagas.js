import { call, put, takeEvery } from "redux-saga/effects";

import NetworkManager from "../../utils/networkManager";
import {actionCreators, actionTypes} from "../actions/authenticationActions";

// *** Sagas ***
export function* loginSaga(action) {
  const { email, password } = action.payload;

  try {
    const user = yield call(NetworkManager.login, email, password);
    yield put(actionCreators.loginFulfilled(user));
  } catch (error) {
    yield put(actionCreators.loginFailed(error));
  }
}

export function* logoutSaga() {
  // NOTE There may be some logout process here but we are skipping that for now, we dont need that for the task
  yield put(actionCreators.logoutFulfilled());
}

// *** Watchers ***
function* watchLoginAction() {
  yield takeEvery(actionTypes.LOGIN, loginSaga);
}

function* watchLogoutAction() {
  yield takeEvery(actionTypes.LOGOUT, logoutSaga);
}

// *** Sagas ***
const sagas = [
  watchLoginAction(),
  watchLogoutAction(),
];

export default sagas;
