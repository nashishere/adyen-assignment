import { call, put } from "redux-saga/effects";

import NetworkManager from "../../utils/networkManager";
import { authenticationActionCreators } from "../index";
import { TEST_USER } from "../../utils/constants";
import {loginSaga, logoutSaga} from "./authenticationSagas";

describe("authenticationSagas", () => {
  it("should login successfully", async () => {
    const mockAction = {
      payload: {
        email: TEST_USER.email,
        password: TEST_USER.password,
      },
    };

    const { password, ...mockResponse } = TEST_USER;

    const generator = loginSaga(mockAction);
    let result;

    result = generator.next();
    expect(result.value).toEqual(call(NetworkManager.login, TEST_USER.email, TEST_USER.password));

    result = generator.next(mockResponse);
    expect(result.value).toEqual(put(authenticationActionCreators.loginFulfilled(mockResponse)));
  });

  it("should login fail", async () => {
    const mockAction = {
      payload: {
        email: "foo",
        password: "bar",
      },
    };

    const mockError = new Error();

    const generator = loginSaga(mockAction);
    let result;

    result = generator.next();
    expect(result.value).toEqual(call(NetworkManager.login, "foo", "bar"));

    result = generator.throw(mockError);
    expect(result.value).toEqual(put(authenticationActionCreators.loginFailed(mockError)));
  });

  it("should logout successfully", async () => {
    const generator = logoutSaga();
    let result;

    result = generator.next();
    expect(result.value).toEqual(put(authenticationActionCreators.logoutFulfilled()));
  });
});
