import { call, put, takeEvery } from "redux-saga/effects";

import NetworkManager from "../../utils/networkManager";
import {actionCreators, actionTypes} from "../actions/resourceActions";

// *** Sagas ***
export function* getWeatherInfoSaga(action) {
  const { city } = action.payload;

  try {
    const data = yield call(NetworkManager.getWeatherInfoFor, city);
    yield put(actionCreators.getWeatherInfoFulfilled(city, data.data));
  } catch (error) {
    yield put(actionCreators.getWeatherInfoFailed(city, error));
  }
}

export function* getFlightsInfoSaga(action) {
  const { city } = action.payload;

  try {
    const data = yield call(NetworkManager.getFlightsInfoFor, city);
    yield put(actionCreators.getFlightsInfoFulfilled(city, data.data));
  } catch (error) {
    yield put(actionCreators.getFlightsInfoFailed(city, error));
  }
}

// *** Watchers ***
function* watchGetWeatherInfoAction() {
  yield takeEvery(actionTypes.GET_WEATHER_INFO, getWeatherInfoSaga);
}

function* watchGetFlightsInfoAction() {
  yield takeEvery(actionTypes.GET_FLIGHTS_INFO, getFlightsInfoSaga);
}

// *** Sagas ***
const sagas = [
  watchGetWeatherInfoAction(),
  watchGetFlightsInfoAction(),
];

export default sagas;
