import { call, put } from "redux-saga/effects";

import NetworkManager from "../../utils/networkManager";
import { resourceActionCreators } from "../index";
import { CITY } from "../../utils/constants";
import { getWeatherInfoSaga, getFlightsInfoSaga } from "./resourceSagas";

describe("resourceSagas", () => {
  it("should get weather info successfully", async () => {
    const mockAction = {
      payload: {
        city: CITY.AMSTERDAM,
      },
    };

    const mockResponse = {
      data: {
        foo: "bar",
      },
    };

    const generator = getWeatherInfoSaga(mockAction);
    let result;

    result = generator.next();
    expect(result.value).toEqual(call(NetworkManager.getWeatherInfoFor, CITY.AMSTERDAM));

    result = generator.next(mockResponse);
    expect(result.value).toEqual(put(resourceActionCreators.getWeatherInfoFulfilled(CITY.AMSTERDAM, mockResponse.data)));
  });

  it("should get weather info fail", async () => {
    const mockAction = {
      payload: {
        city: CITY.AMSTERDAM,
      },
    };

    const mockError = new Error();

    const generator = getWeatherInfoSaga(mockAction);
    let result;

    result = generator.next();
    expect(result.value).toEqual(call(NetworkManager.getWeatherInfoFor, CITY.AMSTERDAM));

    result = generator.throw(mockError);
    expect(result.value).toEqual(put(resourceActionCreators.getWeatherInfoFailed(CITY.AMSTERDAM, mockError)));
  });

  it("should get flight info successfully", async () => {
    const mockAction = {
      payload: {
        city: CITY.AMSTERDAM,
      },
    };

    const mockResponse = {
      data: {
        foo: "bar",
      },
    };

    const generator = getFlightsInfoSaga(mockAction);
    let result;

    result = generator.next();
    expect(result.value).toEqual(call(NetworkManager.getFlightsInfoFor, CITY.AMSTERDAM));

    result = generator.next(mockResponse);
    expect(result.value).toEqual(put(resourceActionCreators.getFlightsInfoFulfilled(CITY.AMSTERDAM, mockResponse.data)));
  });

  it("should get flight info fail", async () => {
    const mockAction = {
      payload: {
        city: CITY.AMSTERDAM,
      },
    };

    const mockError = new Error();

    const generator = getFlightsInfoSaga(mockAction);
    let result;

    result = generator.next();
    expect(result.value).toEqual(call(NetworkManager.getFlightsInfoFor, CITY.AMSTERDAM));

    result = generator.throw(mockError);
    expect(result.value).toEqual(put(resourceActionCreators.getFlightsInfoFailed(CITY.AMSTERDAM, mockError)));
  });
});
