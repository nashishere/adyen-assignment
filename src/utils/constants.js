export const CITY = {
  AMSTERDAM: "AMSTERDAM",
  MADRID: "MADRID",
  BUDAPEST: "BUDAPEST",
};

export const CITY_ID = {
  AMSTERDAM: 2759794,
  MADRID: 3117735,
  BUDAPEST: 3054643,
};

export const CITY_AIRPORT = {
  AMSTERDAM: "airport:AMS",
  MADRID: "airport:MAD",
  BUDAPEST: "airport:BUD",
}

// We can read the values below from the environment as an improvement
export const WEATHER_API_BASE_URL = "https://api.weatherbit.io/v2.0/";
export const WEATHER_API_ACCESS_KEY = "1c7a5c10158444f5bcf38aa22a1813c6";
export const FLIGHTS_API_BASE_URL = "https://api.skypicker.com/";

// This is the test user, should not be in the code normally
export const TEST_USER = {
  id: "0f75df88-94ff-4f50-8b3c-de02b0f0e780",
  firstName: "Jamie",
  lastName: "Doe",
  email: "test@test.com",
  password: "test",
};
