import axios from "axios";
import moment from "moment";

import {
  CITY_AIRPORT,
  CITY_ID,
  FLIGHTS_API_BASE_URL,
  TEST_USER,
  WEATHER_API_ACCESS_KEY,
  WEATHER_API_BASE_URL
} from "./constants";

function NetworkManager() {
  let weatherApi = axios.create({
    baseURL: WEATHER_API_BASE_URL,
  });

  let flightsApi = axios.create({
    baseURL: FLIGHTS_API_BASE_URL,
  });

  return {
    getApiInstancesForTestingPurposes: () => ({
      weather: weatherApi,
      flights: flightsApi,
    }),

    login: (email, password) => {
      // We are simulating the operation here
      return new Promise((resolve, reject) => {
        if (email === TEST_USER.email && password === TEST_USER.password) {
          const { password, ...rest } = TEST_USER;
          const user = { ...rest };

          resolve(user);
        } else {
          reject(new Error("Invalid user credentials!"));
        }
      });
    },

    getWeatherInfoFor: (city) => {
      return weatherApi.get("forecast/daily", {
        params: {
          key: WEATHER_API_ACCESS_KEY,
          city_id: CITY_ID[city],
          days: 7,
        },
      });
    },

    getFlightsInfoFor: (city) => {
      return flightsApi.get("flights", {
        params: {
          v: "3",
          partner: "picky",
          date_from: moment().format("DD/MM/YYYY"),
          date_to: moment().add(1, "day").format("DD/MM/YYYY"),
          fly_from: "airport:IST",
          fly_to: CITY_AIRPORT[city],
          curr: "EUR",
          vehicle_type: "aircraft",
          flight_type: "oneway",
          max_stopovers: 0,
          adults: 1,
          sort: "price",
        },
      });
    },
  };
}

export default NetworkManager();
