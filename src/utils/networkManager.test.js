import axios from "axios";
import moxios from "moxios";

import NetworkManager from "./networkManager";
import {CITY, TEST_USER, WEATHER_API_BASE_URL} from "./constants";

describe("networkManager", () => {
  let instances;

  beforeEach(() => {
    instances = NetworkManager.getApiInstancesForTestingPurposes();

    moxios.install(instances.weather);
    moxios.install(instances.flights);
  });

  afterEach(() => {
    moxios.uninstall(instances.weather);
    moxios.uninstall(instances.flights);
  });

  it("should handle login request", (done) => {
    NetworkManager
      .login(TEST_USER.email, TEST_USER.password)
      .then((user) => {
        const { password, ...expectedUser } = TEST_USER;
        expect(user).toEqual(expectedUser);
      })
      .finally(done);
  });

  it("should fail login request", (done) => {
    NetworkManager
      .login("foo", "bar")
      .catch((error) => {
        expect(error.message).toEqual("Invalid user credentials!");
      })
      .finally(done);
  });

  it("should handle getWeatherInfo request", async (done) => {
    moxios.withMock(() => {
      NetworkManager.getWeatherInfoFor(CITY.AMSTERDAM);

      moxios.wait(() => {
        let request = moxios.requests.mostRecent();

        request.respondWith({
          status: 200,
          response: {},
        }).then((response) => {
          expect(response.status).toEqual(200);
        }).finally(done);
      });
    });
  });

  it("should handle getFlightsInfo request", async (done) => {
    moxios.withMock(() => {
      NetworkManager.getFlightsInfoFor(CITY.AMSTERDAM);

      moxios.wait(() => {
        let request = moxios.requests.mostRecent();

        request.respondWith({
          status: 200,
          response: {},
        }).then((response) => {
          expect(response.status).toEqual(200);
        }).finally(done);
      });
    });
  });
});
