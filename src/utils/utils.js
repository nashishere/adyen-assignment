import {createStore} from "redux";
import validate from "validate.js";

import {combinedReducers} from "../state";

export const validateLoginForm = (parameters) => {
  return validate(
    parameters,
    {
      email: {
        email: true,
      },
      password: {
        presence: {
          allowEmpty: false,
        },
      },
    },
  );
};

export const createMockStore = (state) => {
  return createStore(combinedReducers, state);
};
